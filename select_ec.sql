use shop;
-- используем бд shop
-- показать всю таблицу категории
SELECT * FROM category;
-- показать только те столбцы у которых id 3
SELECT * FROM category WHERE id = 3;
-- показать у кого скидка не равен 0
SELECT * FROM category WHERE discount <> 0;
SELECT * FROM category WHERE discount > 5;
SELECT * FROM category WHERE (discount > 5) AND (discount < 15);
SELECT * FROM category WHERE (discount < 5) OR (discount >= 11);
-- показать строку где скидки не меньше 5
SELECT * FROM category WHERE NOT(discount < 5);
-- если псевдоним не NULL
SELECT * FROM category WHERE aliase_name IS NOT NULL;
SELECT * FROM category WHERE aliase_name IS NULL;
-- только столбец название
SELECT name FROM category;
-- название и скидки
SELECT name, discount FROM category;
SELECT discount, name FROM category;
SELECT discount FROM category;
-- только уникальные скидки без дубликатов
SELECT distinct discount FROM category;
-- вывести все категории товаров и отсортировать их по размеру скидки ASC можно не указывать
SELECT * FROM category ORDER BY discount; -- ASC;
-- для сортировки в обратном порядке
SELECT * FROM category ORDER BY discount DESC;
-- вывести все категории товаров с ненулевой скидкой и отсортировать их по р-ру скидки в обратном порядке
SELECT * FROM category WHERE discount <> 0 ORDER BY discount DESC;
-- получить первые две строки not good
SELECT * FROM category WHERE id < 3;
-- good
SELECT * FROM category WHERE discount <> 0 LIMIT 2;
-- изментять значения
UPDATE category SET name = 'dresses' WHERE id = 1;
SELECT * FROM category ;
UPDATE category SET discount = 3 WHERE discount = 0; -- error 
UPDATE category SET discount = 3 WHERE id = 5 ;
UPDATE category SET discount = 4 WHERE id IN (2, 5) ;
SELECT * FROM category ;
DELETE FROM category WHERE id = 5;
