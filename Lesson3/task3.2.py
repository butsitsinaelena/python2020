"""2. Написать функцию, которая принимает произвольное количество любых аргументов.
Аргументами могут быть вложенные списки и кортежи, содержащие числа и другие списки и кортежи.
Пример вызова функции: foo(1, 2, [3, 4, (5, 6, 0)], a=(10, 11), b=(3, 4, [5, 6, [7, 8], []]))
Функция должна вернуть произведение и сумму всех ненулевых элементов вложенных чисел.
Возможны циклические ссылки в аргументах. Пример такого аргумента: a = [1, 2, 3]; a.append(a)
При обнаружении циклической ссылки нужно сообщить пользователю и вернуть None."""


def get_sum_comp(*args, **kwargs):
    tuple_param = (args, tuple(kwargs.values()))
    links_list = []

    def get_args(param, links):
        elm_sum = 0
        elm_comp = 1
        for elm in param:
            if isinstance(elm, int) or isinstance(elm, float):
                if elm != 0:
                    elm_sum += elm
                    elm_comp *= elm
            elif isinstance(elm, list) or isinstance(elm, tuple):
                if elm in links:
                    print("circular reference detected: {}".format(elm))
                    return None, None
                links.append(elm)
                t = get_args(elm, links)
                if t[0] is None or t[1] is None:
                    return None, None
                elm_sum += t[0]
                elm_comp *= t[1]
        return elm_sum, elm_comp
    return get_args(tuple_param, links_list)


d = [1, 2]
d.append(d)
result1 = get_sum_comp(1, 2, [3, 4, (5, 6.7, 0)],  a=(10, 11), b=(3, 4, [5, 6, [7, 8], d, []]))
print('result1 = ', result1)
result2 = get_sum_comp(1, 34.5, 7)
print('result2 = ', result2)
result3 = get_sum_comp([1, -8, 30], s=(2, d))
print('result3 = ', result3)


