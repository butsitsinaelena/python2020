"""3. Написать функцию, которая будет принимать только 4 позиционных аргументы
(все аргументы числовые).Функция должна вернуть среднее арифметическое аргументов
и самый большой аргумент за все время вызовов этой функции.
Пример: foo(1,2,3,4) -> 2.5, 4
        foo(-3, -2, 10, 1) -> 1.5, 10
        foo(7,8,8,1) -> 6, 10"""


def get_average_v1(num1, num2, num3, num4):
    global global_max
    global_max = max(num1, num2, num3, num4, global_max)
    average_value = (num1 + num2 + num3 + num4) / 4
    return average_value, global_max


def maker():
    maker_max = 0

    def get_average_v2(num1, num2, num3, num4):
        nonlocal maker_max
        average_value = (num1 + num2 + num3 + num4) / 4
        maker_max = max(num1, num2, num3, num4, maker_max)
        return average_value, maker_max
    return get_average_v2


print("result get_average_v1_____")
global_max = 0
print(get_average_v1(16, 2, 3, 5))
print(get_average_v1(6, 12, 1, -6))
print(get_average_v1(16, 2, 3, 115))
print("result get_average_v2_____")
get_average_v2_instance = maker()
print(get_average_v2_instance(16, 2, 3, 5))
print(get_average_v2_instance(6, 12, 1, -6))
print(get_average_v2_instance(16, 2, 3, 115))
