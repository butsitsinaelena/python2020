"""1. Написать несколько функций, которые в качестве единственного
аргумента принимают список (или кортеж) целых чисел.
- первая функция должна вернуть квадраты элементов коллекции;
- вторая функция должна вернуть только элементы на четных позициях;
- третья функция возвращает кубы четных элементов на нечетных позициях."""


def get_squares(my_tuple):
    return tuple(map(lambda elem: elem**2, my_tuple))


def get_positions(my_tuple):
    return my_tuple[1:len(my_tuple)+1:2]


def get_cubes(my_tuple):
    cubes = []
    for elem in my_tuple[0:len(my_tuple) + 1:2]:
        if elem % 2 == 0:
            cubes.append(elem**3)
    return tuple(cubes)


my_tup = (1, 2, 3, 4, 5, 10, 2, 45, 6, 7, 8, 9, 5, 4)
result1 = get_squares(my_tup)
result2 = get_positions(my_tup)
result3 = get_cubes(my_tup)

print(result1)
print(result2)
print(result3)
