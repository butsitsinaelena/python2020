"""4. На вход функции передается строка с xml документом (тэги без аттрибутов,
корневой элемент только один).
Нужно выполнить следующее преобразование и вывести максимальную вложенность.
Пример:
    a = '<root>
            <element1 />
            <element2 />
            <element3>
                <element4 />
            </element3>
        </root>'
    foo(a) ->
    {
        'name': 'root',
        'children': [
            {'name': 'element1', 'children': []},
            {'name': 'element2', 'children': []},
            {
                'name': 'element3',
                'children': [
                    {'name': 'element4', 'children': []}
                ]
            }
        ]
    }, 2
в данном случае у element4 тэга вложенность/глубина 2"""

import xml.etree.ElementTree as etree


def parse_tree(node, level):
    node_children = []
    subtree = {'name': node.tag, 'children': node_children}
    current_level = level
    for child in node:
        result = parse_tree(child, level+1)
        nested_level = result[1]
        if nested_level > current_level:
            current_level = nested_level
        node_children.append(result[0])
    return subtree, current_level


def get_pars_obj(xml_string):
    level = 0
    parse_obj = etree.fromstring(xml_string)
    return parse_tree(parse_obj, level)


string_xml = '<root><element1 /><element2 /><element3><element4 /></element3></root>'
tree, depth = get_pars_obj(string_xml)
print("tree = {}, depth = {} ".format(tree, depth))
