"""Разработать класс Student для представления сведений об успешности слушателя некого
курса. Объект класса должен содержать поля для сохранения имени студента и баллов,
полученных им за выполнение практических заданий и финального экзамена.
Обеспечить следующие методы класса:
конструктор, принимающий строку - имя студента - и словарь, содержащий настройки
курса в следующем формате:
conf = {
'exam_max': 30 # количество баллов, доступная за сдачу экзамена
'lab_max' : 7, # количество баллов, доступная за выполнение 1 практической работы
'lab_num': 10 # количество практических работ в курсе
'k': 0.61, # доля баллов от максимума, которую необходимо набрать для получения
сертификата
}
- метод make_lab (m, n),
который принимает 2 аргумента и возвращает ссылку на текущий объект. Здесь m -
количество баллов набрано за выполнение задания (целое или действительное число),
а n - целое неотрицательное число, порядковый номер задания (лабы нумеруются от 0
до lab_num-1). При повторной сдаче задачи засчитывается последняя оценка.
Если n не задано, подразумевается сдача первого невыполненного практического
задания. Учесть, что во время тестирования система иногда дает сбои, поэтому за
выполнение задания может быть начислено больше баллов чем это возможно по
правилам курса, не должно влиять на рейтинг студента.
Кроме того, в системе могут содержаться дополнительные задачи, чьи номера выходят
за пределы 0..lab_num - конечно, баллы за них не должны засчитываться для
получения сертификата.
- метод make_exam (m), который принимает 1 аргумент - целое или действительное число,
оценку за финальный экзамен, и возвращает ссылку на текущий объект. Как и в
случае с практическими задачами, оценка за экзамен в результате ошибки иногда
может превышать максимально допустимую.
- метод is_certified (), который возвращает tuple, содержащий действительное число
(сумму баллов студента за прохождение курса), и логическое значение True или False
в зависимости от того, достаточно ли этих баллов для получения сертификата.
Так как курс доступен онлайн и не имеет дедлайнов на сдачу работ, студент может
выполнять работы в произвольном порядке. Считать, что количество попыток на выполнение
каждого из заданий ограничено."""


class Student:
    exam_attempt = 3
    lab_attempt = 3

    def __init__(self, full_name, conf):
        self.full_name = full_name
        self.exam_max = conf['exam_max']
        self.lab_max = conf['lab_max']
        self.lab_num = conf['lab_num']
        self.k = conf['k']
        self.lab_scores = [0 for i in range(self.lab_num)]
        self.exam_score = 0
        self.exam_attempt = Student.exam_attempt
        self.lab_attempt = [Student.lab_attempt for i in range(self.lab_num)]

    def __str__(self):
        return self.full_name

    def __validate_lab_score(self, lab_score):
        if isinstance(lab_score, int) or isinstance(lab_score, float):
            if lab_score < 0:
                raise ValueError('Laboratory score can not be negative')
            return min(lab_score, self.lab_max)
        else:
            raise TypeError

    @staticmethod
    def __validate_lab_number(lab_number):
        if isinstance(lab_number, int):
            if lab_number < 1:
                raise ValueError('Laboratory number can not be less than one')
        else:
            raise TypeError

    def __get_first_uncompleted_lab(self):
        for i in range(len(self.lab_scores)):
            if self.lab_scores[i] == 0:
                return i + 1

    def make_lab(self, lab_score, lab_number=None):
        if lab_number is None:
            lab_number = self.__get_first_uncompleted_lab()
        self.lab_attempt[lab_number-1] -= 1
        try:
            lab_score = self.__validate_lab_score(lab_score)
            self.__validate_lab_number(lab_number)
        except TypeError:
            return self
        except ValueError:
            return self
        if self.lab_attempt[lab_number-1] >= 0:
            if lab_number <= self.lab_num:
                self.lab_scores[lab_number-1] = lab_score
        return self

    def __validation_exam_score(self, exam_score):
        if isinstance(exam_score, int) or isinstance(exam_score, int):
            if exam_score < 0:
                raise ValueError('Examination score can not be negative')
            return min(exam_score, self.exam_max)
        else:
            raise TypeError

    def make_exam(self, exam_score):
        self.exam_attempt -= 1
        try:
            exam_score = self.__validation_exam_score(exam_score)
        except TypeError:
            return self
        except ValueError:
            return self
        if self.exam_attempt >= 0:
            self.exam_score = exam_score
        return self

    def __score_calc(self):
        return self.exam_score + sum(self.lab_scores)

    def is_certified(self):
        if (self.__score_calc() / 100) >= 0.61:
            return self.__score_calc(), True
        elif (self.__score_calc() / 100) < 0.61:
            return self.__score_calc(), False


student_dict = {'exam_max': 30, 'lab_max': 7, 'lab_num': 10, 'k': 0.61}
student1 = Student('Ivan', student_dict)

student1.make_lab(1).make_lab(2, 2).make_lab(2).make_lab(7, 4)
student1.make_lab(3).make_lab(7, 5).make_lab(5, 5)
student1.make_lab(10, 5)  # попытки кончились, оценку не защитали
student1.make_lab(4, 6)
student1.make_lab(10, 6)  # максимум 7 балов, поэтому 10 не защитали
student1.make_lab(5, 7).make_lab(6).make_lab(7, 9).make_lab(8, 10)
student1.make_lab(5, 3)  # пересдал третью лабу
student1.make_exam(25)
student2 = Student('Vasya', student_dict)
student2.make_lab(2, 7).make_lab(-2).make_lab(44, 1).make_lab(3, -3)
print(student1.lab_scores)
print(student1.is_certified())
print(student2.lab_scores)
print(student2.is_certified())

