"""Разработать класс Complex, которые бы описывал комплексные числа,
позволял их складывать, вычитать, умножать, делить и получать модуль.
Вывод действительной и мнимой частей должен быть с точностью до двух
знаков после запятой.
На вход: действительная и мнимая часть числа, разделенная пробелом.
На выходе:
Для двух комплексных чисел вывод должен быть в следующей
последовательности в отдельных строках:
C + D
C – D
C * D
C/D
mod(C)
mod(D)
P.S. Не забудьте про перегрузку магических методов, пожалуйста"""
import math


class ComplexNumber:

    def __init__(self, real, imag):
        self.real = float(real)  # действительная часть
        self.imag = float(imag)  # мнимая часть

    def __repr__(self):
        return '{:.2f} + {:.2f}i'.format(self.real, self.imag)
    
    def __add__(self, other):
        return ComplexNumber(self.real + other.real, self.imag + other.imag)

    def __sub__(self, other):
        return ComplexNumber(self.real - other.real, self.imag - other.imag)
    
    def __mul__(self, other):
        return ComplexNumber(self.real * other.real, self.imag * other.imag)
    
    def __truediv__(self, other):
        return ComplexNumber(self.real / other.real, self.imag / other.imag)

    def mod(self):
        complex_mod = math.sqrt(self.real**2 + self.imag**2)
        return float('{:.2f}'.format(complex_mod))


comp1 = ComplexNumber(5, 1)
comp2 = ComplexNumber(4, 3)
print('comp1 = ', comp1, type(comp1))
print('comp2 = ', comp2)
comp_add = comp1 + comp2
print('comp_add = ', comp_add)
comp_sub = comp1 - comp2
print('comp_sub =', comp_sub)
comp_mul = comp1 * comp2
print('comp_mul = ', comp_mul)
comp_truediv = comp1 / comp2
print('comp_truediv = ', comp_truediv)
comp3 = ComplexNumber(-4, -5)
print('comp3 = ', comp3)
comp3_mod = comp3.mod()
print('comp3_mod = ', comp3_mod, type(comp3_mod))
comp1_mod = comp1.mod()
print('comp1_mod = ', comp1_mod, type(comp1_mod))
