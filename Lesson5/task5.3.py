class Observable:
    def __init__(self, **kwargs):
        for name in kwargs.keys():
            setattr(self, name, kwargs[name])

    def __get_class_name(self):
        return str(self.__class__.__name__)

    def __repr__(self):
        values_strings = []
        for key in self.__dict__.keys():
            if key.startswith("_"):
                continue
            values_strings.append("{}={}".format(key, self.__dict__[key]))
        values = ", ".join(values_strings)
        return '{}{}{}{}'.format(self.__get_class_name(), "(", values, ")")


class NewObservable(Observable):
    pass


s = NewObservable(foo=1, bar=5, _bazz=12, name='Amok', props=('One', 'two'))
print(s.foo)
print(s.bar)
print(s._bazz)
print(s.name)
print(s.props)
print(s)
