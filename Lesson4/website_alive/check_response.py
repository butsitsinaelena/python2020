from Lesson4.website_alive.make_request import make_req


def check_request(url_string):
    try:
        response = make_req(url_string)
        return response.status_code == 200
    except:
        return False
