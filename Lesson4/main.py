from Lesson4.website_alive import check_response


WEBSITE = input('enter the address to verify: ')
RESPONSE = check_response.check_request(WEBSITE)
if RESPONSE is True:
    print("Website {} is available".format(WEBSITE))
else:
    print("Website {} does not work or does not exist".format(WEBSITE))
