from time import time


# def timeit(func):
#     def wapper():
#         start = time()
#         result = func()
#         print(time() - start)
#         return result
#     return wapper


def timeit(arg):
    print(arg)

    def outer(func):
        def wrapper(*args, **kwargs):
            start = time()
            result = func(*args, **kwargs)
            print(time() - start)
            return result
        return wrapper
    return outer


@timeit('name')
def one(n):
    lst = []
    for i in range(n):
        if i % 2 == 0:
            lst.append(i)
    return lst


@timeit('name')
def two(n):
    lst = [x for x in range(n) if x % 2 == 0]
    return lst


# l1 = one(10**4)
# l2 = two(10**4)
# print(l1)
# print(l2)
# l1 = one  # не вызываем функциюБ а передаем ее как обьект
# a = l1(10)
# print(a)
# l1 = timeit(one)
# l2 = timeit(one)(10)  #  => wrapper(10) => one(10)
# # print(type(l1), l1.__name__)
# a = l1(10)
# print(a)

one(10)
two(10)
