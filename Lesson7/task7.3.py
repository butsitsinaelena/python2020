"""3. Каррирование.
Преобразовать вызов функции с конечным количеством
позиционных аргументов f(a, b, c, d) в вызов вида
f(a)(b)(c)(d), используя декоратор.
Пример:
@carry
def foo(a, b):
    return a + b
foo(1)(5)  # вернет 6
"""


def carry(func):
    def wrapper(arg1):
        def outer(arg2):
            def inner(arg3):
                return func(arg1, arg2, arg3)
            return inner
        return outer
    return wrapper


@carry
def foo(a, b, c):
    return a + b + c


print(foo(4)(2)(1))
