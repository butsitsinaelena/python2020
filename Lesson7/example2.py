import time
from functools import wraps


def get_method(func):
    @wraps(func)
    def decor_method(self, *args, **kwargs):
        start = time.time()
        result = func(self, *args, **kwargs)
        end = time.time() - start
        return result, print('Method "{}" run time= {}'.format(func.__name__, end))

    return decor_method


def time_methods(*methods):

    def inner_decorator(decor_class):
        getattribute = decor_class.__getattribute__
        count = 0
        decorated = []

        @wraps(decor_class)
        def wrapper_class(self, *args, **kwargs):
            nonlocal count
            nonlocal decorated
            for method in methods:
                if hasattr(decor_class, method) and method not in decorated:
                    atr = getattribute(decor_class, method)
                    call = get_method(atr)
                    count += 1
                    print('count= ', count)
                    print('call= {}, {}'.format(get_method(atr), decor_class))
                    setattr(decor_class, method, call)
                    decorated.append(method)
            return decor_class(self, *args, **kwargs)

        return wrapper_class
    return inner_decorator


@time_methods('inspect', 'finalize111', 'get_print')
class Spam:
    def __init__(self, attr):
        self.attr = attr

    def inspect(self):
        return time.sleep(self.attr)

    def get_print(self):
        return time.sleep(self.attr*2)

    def foo(self):
        return self.attr


a = Spam(2)
b = Spam(2)
c = Spam(2)

a.inspect()
a.get_print()  #  должно вывести сообщение о времени работы
b.get_print()
c.get_print()


# a.foo()  # ничего не выводить"""
# a.get_print()
# b.inspect()
# b.foo()


##################################################


# a.foo()  # ничего не выводить"""
# a.get_print()
# b.inspect()
# b.foo()


import time
from functools import wraps

#
# def get_method(func):
#     @wraps(func)
#     def decor_method(*args, **kwargs):
#         start = time.time()
#         result = func(*args, **kwargs)
#         end = time.time() - start
#         #print('Method "{}" run time= {}'.format(func.__name__, end))
#         return result, print('Method "{}" run time= {}'.format(func.__name__, end))
#
#     return decor_method
#
#
# def time_methods(*methods):
#
#     def inner_decorator(decor_class):
#         getattribute = decor_class.__getattribute__
#         count = 0
#
#         @wraps(decor_class)
#         def wrapper_class(self, *args, **kwargs):
#             nonlocal count
#             for method in methods:
#                 if hasattr(decor_class, method):
#                     atr = getattribute(decor_class, method)
#                     call = get_method(atr)
#                     count += 1
#                     print('count= ', count)
#                     print('call= {}, {}'.format(get_method(atr), decor_class))
#                     setattr(decor_class, method, call)
#             return decor_class(*args, **kwargs)
#
#         return wrapper_class
#     return inner_decorator
#
#
# @time_methods('inspect', 'finalize111', 'get_print')
# class Spam:
#     def __init__(self, attr):
#         self.attr = attr
#
#     def inspect(self):
#         return time.sleep(self.attr)
#
#     def get_print(self):
#         return time.sleep(self.attr*2)
#
#     def foo(self):
#         return self.attr

import time
from functools import wraps


def get_method(func):
    @wraps(func)
    def decor_method(self, *args, **kwargs):
        start = time.time()
        result = func(self, *args, **kwargs)
        end = time.time() - start
        #print('Method "{}" run time= {}'.format(func.__name__, end))
        return result, print('Method "{}" run time= {}'.format(func.__name__, end))

    return decor_method


def time_methods(*methods):

    def inner_decorator(decor_class):
        getattribute = decor_class.__getattribute__
        count = 0

        @wraps(decor_class)
        def wrapper_class(self, *args, **kwargs):
            nonlocal count
            for method in methods:
                if hasattr(decor_class, method):
                    atr = getattribute(decor_class, method)
                    call = get_method(atr)
                    count += 1
                    print('count= ', count)
                    print('call= {}, {}'.format(get_method(atr), decor_class))
                    setattr(decor_class, method, call)
            return decor_class(self, *args, **kwargs)

        return wrapper_class
    return inner_decorator


@time_methods('inspect', 'finalize111', 'get_print')
class Spam:
    def __init__(self, attr):
        self.attr = attr

    def inspect(self):
        return time.sleep(self.attr)

    def get_print(self):
        return time.sleep(self.attr*2)

    def foo(self):
        return self.attr


a = Spam(2)
b = Spam(3)
# # #aa = Spam(1)
a.inspect()  #  должно вывести сообщение о времени работы
# a.foo()  # ничего не выводить"""
# a.get_print()
# b.inspect()
# b.foo()