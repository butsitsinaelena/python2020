def trace1(func):
    def inner1(*args, **kwargs):
        print(func.__name__, args, kwargs)
        return func(*args, **kwargs)

    return inner1


# @trace
def identity1(x):
    """I do nothing useful."""
    return x


RES = identity1(42)
print(identity1(10))
print("1. вызываем help, части атрибутов ф-ии identity нет. "
      "декорированная ф-я не имеет части атрибутов оригинальной")
help(identity1)
print('identity1.__name__ до декорирования: ', identity1.__name__)  # => identity1
print('identity1.__doc__ до декорирования: ', identity1.__doc__)  # если не декорировать функцию => I do nothing useful.
print('identity1.__module__ до декорирования: ', identity1.__module__)  # если не декорировать функцию => __main__

identity1 = trace1(identity1)
print('identity1.__name__,  identity1.__doc__ после декорирования: ',
      identity1.__name__, identity1.__doc__)  # после декорирования => inner1, None
print('identity1.__module__ после декорирования: ', identity1.__module__)  # => __main__
###################################################################################
print('первый вариант решения востановление документов: ')


def trace2(func):
    def inner2(*args, **kwargs):
        print(func.__name__, args, kwargs)
        return func(*args, **kwargs)

    inner2.__module__ = func.__module__
    inner2.__name__ = func.__name__
    inner2.__doc__ = func.__doc__
    return inner2


@trace2
def identity2(x):
    """I do nothing useful."""
    return x


print('help start_______________: ')
help(identity2)
print('help end_________________: ')
print('identity2.__name__, identity2.__doc__, result1 = ', identity2.__name__, identity2.__doc__)
# после декорирования => identity, I do nothing useful.
print('identity2.__module__, result2 = ', identity2.__module__)  # => __main__
###################################################################################################
print('второй вариант решения проблеммы потери докумантов: ')
import functools


def trace3(func):
    def inner3(*args, **kwargs):
        print(func.__name__, args, kwargs)
        return func(*args, **kwargs)

    functools.update_wrapper(inner3, func)
    return inner3


@trace3
def identity3(x):
    """I do nothing useful."""
    return x


print('help start_______________: ')
help(identity3)
print('help end_________________: ')
print('result3 = ', identity3.__name__, identity3.__doc__)  # после декорирования => identity3, I do nothing useful.
print('result4 = ', identity3.__module__)  # => __main__
#########################################################################
print('третий вариант решения проблеммы потери докумантов, похож на вариант 2: ')


def trace4(func):
    @functools.wraps(func)
    def inner4(*args, **kwargs):
        print(func.__name__, args, kwargs)
        return func(*args, **kwargs)

    return inner4


@trace4
def identity4(x):
    """I do nothing useful."""
    return x


print('help start_______________: ')
help(identity4)
print('help end_________________: ')
print('result5 = ', identity4.__name__, identity4.__doc__)  # после декорирования => identity4, I do nothing useful.
print('result6 = ', identity4.__module__)  # => __main__
##############################################################
print('2. возможность глобального отключения trace')
trace_enable = False  # при компеляции в байткод


def trace5(func):
    @functools.wraps(func)
    def inner5(*args, **kwargs):
        print(func.__name__, args, kwargs)
        return func(*args, **kwargs)

    return inner5 if trace_enable else func


@trace5
def identity5(x):
    """I do nothing useful."""
    return x


print('help start_______________: ')  # декорирована: Help on function inner in module __main__:
# отключена: Help on function identity in module __main__:
help(identity5)
print('help end_________________: ')
print('result7 = ', identity5.__name__, identity5.__doc__)  # после декорирования => identity, I do nothing useful.
print('result8 = ', identity5.__module__)  # => __main__
###############################################################################################
print('3 куда писать вызовы. по умолчанию все пишется в stdout')
import sys


def trace6(handle):
    def decorator6(func):
        @functools.wraps(func)
        def inner6(*args, **kwargs):
            print(func.__name__, args, kwargs, file=handle)
            return func(*args, **kwargs)

        return inner6

    return decorator6


@trace6
def identity6(x):
    return x


#############################################
print('декоратор с тройной вложенностью и дофига всего')


def with_arguments7(deco):
    @functools.wraps(deco)
    def wrapper7(*dargs, **dkwargs):
        def decorator7(func):
            result = deco(func, *dargs, **dkwargs)
            functools.update_wrapper(result, func)
            return result

        return decorator7

    return wrapper7


@with_arguments7
def trace7(func, handle):
    def inner7(*args, **kwargs):
        print(func.__name__, args, kwargs, file=handle)
        return func(*args, **kwargs)

    return inner7


@trace7(sys.stderr)
def identity(x):
    return x


print(' как писать проще без тройной вложенности и прочего')


def trace8(func8=None, *, handle=sys.stdout):
    # со скобками, с аргументами у декоратора
    if func8 is None:
        return lambda func: trace8(func8, handle=handle)
    # без скобок, без аргументов у декоратора

    @functools.wraps(func8)
    def inner8(*args, **kwargs):
        print(func8.__name__, args, kwargs)
        return func8(*args, **kwargs)
    return inner8


@trace8(handle=sys.stderr)
def identity8(x):
    return x


identity8(12)
print('Дополним значение по умолчанию')
