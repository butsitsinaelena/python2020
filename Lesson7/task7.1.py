import time


def average_time(n=2):
    def outer(func):
        time_list = []

        def wrapper(*args):
            start = time.time()
            result = func(*args)
            end = round(time.time() - start)
            time_list.append(end)
            if len(time_list) > n:
                time_list.pop(0)
            aver_time = sum(time_list) / len(time_list) * 1000
            return result, "Среднее время работы: {} мс.".format(aver_time)
        return wrapper
    return outer


@average_time(n=2)
def foo(number):
    time.sleep(number)
    return number


print('{}\n{}\n{}\n{}\n{}'.format(foo(3), foo(7), foo(1), foo(4), foo(4)))
