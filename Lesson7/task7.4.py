"""Написать декоратор, который будет проверять тип
аргументов при вызове функции согласно аннотации
функции.Декорирование функции без аннотации или с
неполнойаннотацией (когда аннотированы не все
аргументы) должно рейзить ошибку. В случае
несовпаденияпереданных во время вызова функции
аргументов стипами аргументов в аннотации
- выводить сообщение."""


def check_type(func):
    def wrapper(*args):
        var = func.__annotations__
        if len(var) <= len(args):
            raise ValueError(
                'Function without annotation or with incomplete annotation'
            )
        args_wrap = args
        for elm in tuple(zip(args_wrap, var)):
            if not isinstance(elm[0], var[elm[1]]):
                print('func {}: arg {} -> {} not consistent with annotation {}'.
                      format(func.__name__, elm[0], type(elm[0]), var[elm[1]]))
        if not isinstance(func(*args), var['return']):
            print('func {}: return {} -> {} not consistent with annotation {}'.
                  format(func.__name__, func(*args), type(func(*args)), var['return']))

        return func(*args)
    return wrapper


@check_type
def repeater(s: str, n: int) -> str:
    return s * n


@check_type
def repeater1(num: str) -> str:
    return num


RESULT_REPEATER = repeater('abc', 2)
RESULT_REPEATER1 = repeater1(11)
print(RESULT_REPEATER)
print(RESULT_REPEATER1)
