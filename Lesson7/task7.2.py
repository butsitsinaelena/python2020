"""Напишите параметризованный декоратор для классов,
который будет считать и выводить время работы методов
класса, имена которых переданы в параметрах декоратора.
Пример:
  @time_methods('inspect', 'finalize')
  class Spam:
      def __init__(self, s):
          self.s = s
      def inspect(self):
           sleep(self.s)
      def foo(self):
           return self.s
 a = Spam(2)
 a.inspect()  #  должно вывести сообщение о времени работы
 a.foo()  # ничего не выводить"""

import time
from functools import wraps


def get_method(func):
    @wraps(func)
    def decor_method(*args, **kwargs):
        start = time.time()
        result = func(*args, **kwargs)
        end = time.time() - start
        print('Method "{}" run time= {}'.format(func.__name__, end))
        return result
    return decor_method


def time_methods(*methods):
    def inner_decorator(decor_class):
        getattribute = decor_class.__getattribute__
        decorated = []

        @wraps(decor_class)
        def wrapper_class(*args, **kwargs):
            for method in methods:
                if hasattr(decor_class, method) and method not in decorated:
                    atr = getattribute(decor_class, method)
                    call = get_method(atr)
                    setattr(decor_class, method, call)
                    decorated.append(method)
            return decor_class(*args, **kwargs)

        return wrapper_class
    return inner_decorator


@time_methods('inspect', 'finalize111', 'get_time')
class Spam:
    def __init__(self, attr):
        self.attr = attr

    def inspect(self):
        return time.sleep(self.attr)

    def get_time(self):
        return time.sleep(self.attr+1)

    def foo(self):
        return self.attr


OBJ1 = Spam(2)
OBJ2 = Spam(2)
OBJ3 = Spam(2)
OBJ1.inspect()
OBJ1.get_time()
OBJ1.foo()
OBJ2.get_time()
OBJ3.get_time()

