# class Decorator:
#     def __init__(self, func):
#         self.func = func
#         self.count = 0
#
#     def __call__(self, *args, **kwargs):
#         self.count += 1
#         return self.func(*args, **kwargs)
#
#
# @Decorator
# def foo(a):
#     print(f'called with {a}')
#     return a
#
#
# foo(10)
# foo(12)
# print(foo.count)
# print(type(foo))

############_______управляемые атрибуты___________#############

#
# class Person:
#     def __init__(self, name):
#         self._name = name
#
#     def getName(self):
#         print('fetch...')
#         return self._name
#
#     def setName(self, value):
#         print('Change...')
#         self._name = value
#
#     def delName(self):
#         print('remove...')
#         del self._name
#     name = property(getName, setName, delName, '')


def ValidString(attr_name, empty_allowed = True):
    def decorator(cls):
        name = '__' + attr_name

        def getter(self):
            return getattr(self, name)


    # def carry(func):
    #
    #     def wrapper(*args):
    #         args = tuple(args)
    #         if len(args) == 0:
    #             return func(*args)
    #         args = args[1:]
    #         return wrapper(*args)
    #     return wrapper
    #
    #
    # @carry
    # def foo(a, b, c, d):
    #     return a + b + c + d
    #
    #
    # print(foo(4)(2)(1)(2))
    # #
    # #
    # # def carry(func):
    # #     def wrapper(arg1):
    # #         def inner(arg2):
    # #             return func(arg1, arg2)
    # #         return inner
    # #     return wrapper
    # #
    # #
    # # @carry
    # # def foo(a, b):
    # #     return a + b
    # #
    # #
    # # print(foo(4)(2))
    # #
    # #
    # # # def foo(a):
    # # #     def f(b):
    # # #         return a + b
    # # #     return f
    # # #
    # # # print(foo(1)(2))
    # # # #print(foo(1, 2))
    # #
    # #
    # # def carry(func, *args):
    # #     def wrapper(arg1):
    # #         def inner(arg2):
    # #             def dec1(arg3):
    # #                 return func(arg1, arg2, arg3)
    # #             return dec1
    # #         return inner
    # #     return wrapper
    # #
    # #
    # # @carry
    # # def foo(a, b, c):
    # #     return a + b + c
    # #
    # #
    # # print(foo(4)(2)(-2))
