class FirstClass:
    def set_data(self, value):
        self.data = value

    def display(self):
        return print(self.data)


print('first class_________________')
x = FirstClass()  # создаются два экземпляра
y = FirstClass()  # каждый является отдельным пространством имен
x.set_data('King Arthur')  # вызов метода self - это х
y.set_data(3.14)  # эквивалентно FirstClass.set_data(y, 3.14)
x.display()  # в каждом экземпляре свое значение set_data
y.display()
x.data = 'New value'  # можно получать/записывать значения атрибутов
x.display()  # и за пределами класса тоже
x.anotrer_name = "spam"  # присвоили новый атрибут с именем, который
# затем сможет использоваться любыми методами класса в обьекте экземпляра ч
print(x.anotrer_name)

print('second class_________________')


class SecondClass(FirstClass):  # наследуем set_data
    def display(self):          # изменяем display
        print('Current value = "%s"' % self.data)


z = SecondClass()
z.set_data(42)
z.display()  # Current value = "42"
x.display()  # New value

print('ThirdClass_____________')


class ThirdClass(SecondClass):  # наследует SecondClass
    def __init__(self, value):  # вызывается из ThirdClass(value)
        self.data = value

    def __add__(self, other):
        return ThirdClass(self.data + other)

    def __str__(self):  # вызывается из print(self), str()
        return '[ThirdClass: {}]'.format(self.data)

    def mul(self, other):  # изменяет сам обьект: обычный метод
        self.data *= other


a = ThirdClass('abc')
a.display()  # унаследоват от SecondClass - Current value = "abc"
print(a)  # [ThirdClass: abc]
b = a +'xyz'  # новый __add__: создает ноый экземпляр
b.display()  # Current value = "abcxyz"
print(b)  # [ThirdClass: abcxyz]
a.mul(3)  # изменяет сам экземпляр
print(a)  # [ThirdClass: abcabcabc]

print('simple class_____')


class Rec:  # нет никаких методов. можно присоединять атрибуты используя присваивании
    pass


Rec.name = 'Bob'
Rec.age = 40
print(Rec.name)
print(Rec.age)
xx = Rec()
yy = Rec()
print(Rec.name, xx.name, yy.name)  # Bob Bob Bob
xx.name = 'Sue'
print(Rec.name, xx.name, yy.name)  # Bob Sue Bob
yy.name = 'Martin'
print(Rec.name, xx.name, yy.name)  # Bob Sue Martin
zz = Rec()
print(Rec.__dict__.keys())  # dict_keys(['__module__', '__dict__', '__weakref__', '__doc__', 'name', 'age'])
print(list(xx.__dict__.keys()))  # ['name']
print(list(zz.__dict__.keys()))  # []
print(xx.__class__)  # <class '__main__.Rec'>
print(Rec.__bases__)  # (<class 'object'>,)

print('non class function__________')


def upper_name(self):
    return self.name.upper()


print(upper_name(xx))

# если присвоить атрибуту класса она станет методом
Rec.method = upper_name
print(xx.method())  # SUE
print(yy.method())  # MARTIN
print(zz.method())  # BOB
print(Rec.method(xx))  # вызываем через мя экземпляра или класса  - SUE

print(' используем класс как словарь ______')
Rec = {}
Rec['name'] = 'mel'
Rec['age'] = 34
Rec['job'] = 'trainer / writer'
print(Rec['name'])  # mel

print(' более полноценный класс _______')


class Person:
    def __init__(self, name, job, age):
        self.name = name
        self.job = job
        self.age = age

    def info(self):
        return self.name, self.job, self.age


rec1 = Person('Nik', 'non', 23)
rec2 = Person('Lena', 'developer', 30)
print(rec1.info())  # ('Nik', 'non', 23)
print(rec2.info())  # ('Lena', 'developer', 30)
print(rec2.job, rec1.age)