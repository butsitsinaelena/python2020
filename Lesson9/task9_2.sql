-- employee with salary < 45000
SELECT e.*, p.salary
FROM employee e
LEFT JOIN positiones p
ON e.id_position = p.id_position
WHERE p.salary < 45000;
-- engineer with salary < 30000
SELECT e.*, p.salary
FROM employee e
LEFT JOIN positiones p
ON e.id_position = p.id_position
WHERE (p.id_position = 1 or p.id_position = 2) and p.salary < 30000;
