CREATE SCHEMA company;
-- create tables: employee
CREATE TABLE company.employee (
id_employee INT(11) NOT NULL,
first_name VARCHAR(128) NOT NULL,
last_name VARCHAR(128) NOT NULL,
id_position INT NOT NULL, 
PRIMARY KEY (id_employee));
-- create tables: positiones
CREATE TABLE company.positiones (
id_position INT(11) NOT NULL AUTO_INCREMENT,
name VARCHAR(128) NOT NULL,
salary DECIMAL(10,2) NOT NULL, 
PRIMARY KEY (id_position));
-- add data to tables: positiones
USE company;
INSERT INTO positiones (name, salary ) VALUES ('engineer', '30000');
INSERT INTO positiones (name, salary ) VALUES ('engineer', '27000');
INSERT INTO positiones (name, salary ) VALUES ('programmer', '50000');
INSERT INTO positiones (name, salary ) VALUES ('accountant', '45000');
INSERT INTO positiones (name, salary ) VALUES ('director', '145000');
INSERT INTO positiones (name, salary ) VALUES ('first_manager', '90000');
INSERT INTO positiones (name, salary ) VALUES ('second_manager', '75000');
-- create fk_id_company_possition
USE company;
ALTER TABLE employee 
ADD CONSTRAINT fk_id_position
FOREIGN KEY (id_position)
REFERENCES positiones (id_position)
ON DELETE CASCADE
ON UPDATE NO ACTION;
-- add data to tables: employee
INSERT INTO employee ( id_employee, first_name, last_name, id_position ) VALUES (2354, 'Ivan', 'Ivanov', 1);
INSERT INTO employee ( id_employee, first_name, last_name, id_position ) VALUES (1334, 'Petrov', 'Petr', 2);
INSERT INTO employee ( id_employee, first_name, last_name, id_position ) VALUES (2323, 'Sergeev', 'Sergey', 2);
INSERT INTO employee ( id_employee, first_name, last_name, id_position ) VALUES (2325, 'Sergeev', 'Sergey', 1);
INSERT INTO employee ( id_employee, first_name, last_name, id_position ) VALUES (1111, 'Rodionova', 'Tatyana', 4);
INSERT INTO employee ( id_employee, first_name, last_name, id_position ) VALUES (4545, 'Serova', 'Anna', 3);
INSERT INTO employee ( id_employee, first_name, last_name, id_position ) VALUES (6787, 'Trusov', 'Daniil', 1);
INSERT INTO employee ( id_employee, first_name, last_name, id_position ) VALUES (6785, 'Trusov', 'Daniil', 3);
INSERT INTO employee ( id_employee, first_name, last_name, id_position ) VALUES (6785, 'Trusov', 'Daniil', 3);
INSERT INTO employee ( id_employee, first_name, last_name, id_position ) VALUES (0001, 'Gendir', 'Alex', 5);
INSERT INTO employee ( id_employee, first_name, last_name, id_position ) VALUES (0002, 'Ikota', 'Roman', 6);
INSERT INTO employee ( id_employee, first_name, last_name, id_position ) VALUES (0003, 'Ivanov', 'Daniil', 7);