-- create tables: structure
CREATE TABLE company.structure (
id_boss INT,
id_employee INT,
PRIMARY KEY (id_boss, id_employee),
CONSTRAINT fk_id_boss_employee
FOREIGN KEY (id_boss)
REFERENCES company.positiones (id_position)
ON DELETE NO ACTION
ON UPDATE NO ACTION,
CONSTRAINT fk_id_employee
FOREIGN KEY (id_employee)
REFERENCES company.positiones (id_position)
ON DELETE NO ACTION
ON UPDATE NO ACTION);
-- fill data
INSERT INTO company.structure (id_boss, id_employee) VALUES (5, 1);
INSERT INTO company.structure (id_boss, id_employee) VALUES (5, 2);
INSERT INTO company.structure (id_boss, id_employee) VALUES (5, 3);
INSERT INTO company.structure (id_boss, id_employee) VALUES (5, 4);
INSERT INTO company.structure (id_boss, id_employee) VALUES (5, 6);
INSERT INTO company.structure (id_boss, id_employee) VALUES (5, 7);
INSERT INTO company.structure (id_boss, id_employee) VALUES (6, 7);
INSERT INTO company.structure (id_boss, id_employee) VALUES (7, 1);
-- select all employees reporting particular employee
SELECT boss.first_name as boss_name, boss.last_name as boss_last_name, boss_pos.name as boss_position, e.first_name, e.last_name, e_pos.name as employee_position
FROM employee boss
LEFT JOIN structure c
ON boss.id_position = c.id_boss
LEFT JOIN positiones boss_pos
ON c.id_boss = boss_pos.id_position
LEFT JOIN employee e
ON e.id_position = c.id_employee
LEFT JOIN positiones e_pos
ON e.id_position = e_pos.id_position
WHERE boss.id_employee = 3
