CREATE SCHEMA company;
-- create tables: people, positiones, worker
CREATE TABLE company.people (
personnel_number INT NOT NULL,
first_name VARCHAR(128) NOT NULL,
last_name VARCHAR(128) NOT NULL,
PRIMARY KEY (personnel_number));
CREATE TABLE company.positiones (
id INT NOT NULL, 
name VARCHAR(128) NOT NULL,
selary DECIMAL(10,2) NOT NULL, 
PRIMARY KEY (id)); 
CREATE TABLE company.worker (
id_worker INT NOT NULL,
id_people INT NOT NULL,
id_company_possition INT NOT NULL,
PRIMARY KEY (id_worker));
-- add data to tables: people, positiones
USE company;  
ALTER TABLE people
CHANGE COLUMN personnel_number personnel_number INT(11) NOT NULL AUTO_INCREMENT ;
INSERT INTO people ( irst_name, last_name ) VALUES ('Ivan', 'Ivanov');
INSERT INTO people ( first_name, last_name ) VALUES ('Petrov', 'Petr');
INSERT INTO people (first_name, last_name ) VALUES ('Sergeev', 'Sergey');
INSERT INTO people (first_name, last_name ) VALUES ('Rodionova', 'Tatyana');
INSERT INTO people (first_name, last_name ) VALUES ('Serova', 'Anna');
INSERT INTO people (first_name, last_name ) VALUES ('Trusov', 'Daniil');
ALTER TABLE positiones
CHANGE COLUMN id id INT(11) NOT NULL AUTO_INCREMENT ;
INSERT INTO positiones (name, selary ) VALUES ('engineer', '30000');
INSERT INTO positiones (name, selary ) VALUES ('programmer', '50000');
INSERT INTO positiones (name, selary ) VALUES ('accountant', '45000');

USE company;
-- automation to kreate id_worker
ALTER TABLE worker
CHANGE COLUMN id_worker id_worker INT(11) NOT NULL AUTO_INCREMENT ;
-- create fk_id_people
ALTER TABLE worker 
ADD CONSTRAINT fk_id_people
FOREIGN KEY (id_people)
REFERENCES people (personnel_number)
ON DELETE NO ACTION
ON UPDATE NO ACTION;
-- create fk_id_company_possition
ALTER TABLE worker 
ADD CONSTRAINT fk_id_company_possition
FOREIGN KEY (id_company_possition)
REFERENCES positiones (id)
ON DELETE NO ACTION
ON UPDATE NO ACTION;
-- add data to tables: worker
INSERT INTO worker (id_people, id_company_possition) VALUES (1, 1);
INSERT INTO worker (id_people, id_company_possition) VALUES (2, 2);
INSERT INTO worker (id_people, id_company_possition) VALUES (3, 1);
INSERT INTO worker (id_people, id_company_possition) VALUES (2, 1);
INSERT INTO worker (id_people, id_company_possition) VALUES (5, 2);
INSERT INTO worker (id_people, id_company_possition) VALUES (4, 3);
INSERT INTO worker (id_people, id_company_possition) VALUES (6, 2); -- error
INSERT INTO worker (id_people, id_company_possition) VALUES (4, 4); -- error

