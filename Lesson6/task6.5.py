"""Написать функцию-генератор chain, которая последовательно итерирует переданные объекты
(произвольное количество)"""


def chain(*args):
    for arg in args:
        if isinstance(arg, int):
            arg = [arg]
        if isinstance(arg, dict):
            for key, value in arg.items():
                yield key, value
        else:
            for elm in arg:
                yield elm


obj1 = 'abcd'
obj2 = [1, 2, 3]
obj3 = (11, 12, 14, 16)
obj4 = 123
obj5 = {'onion': 12, 'apple': 23, 'banana': 13, 'bread': 45, 'milk': 12}
obj6 = set('onion')
obj7 = frozenset('apple')
it = chain(obj1, obj2, obj3, obj4, obj5, obj6, obj7)

while True:
    try:
        print(next(it))
    except StopIteration:
        print("\nStopIteration: iterable object is the end")
        break
