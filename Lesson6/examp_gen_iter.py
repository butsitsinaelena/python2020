res = []
for x in 'spam':
    res.append(ord(x))
print(res)  # коды ASCII  строки  'spam' - [115, 112, 97, 109]

res = map(ord, 'spam')  # то же что и в предыдущей, только короче и проще
print(list(res))  # коды ASCII  строки  'spam' - [115, 112, 97, 109]

res = [ord(x) for x in 'spam']  # получаем с помощью генератора списков
print(res)  # коды ASCII  строки  'spam' - [115, 112, 97, 109]

res = [x +y for x in [0, 1, 2] for y in [100, 200, 300]]
print(res)