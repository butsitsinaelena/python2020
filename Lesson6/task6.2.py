"""Написать генератор списка для получения списка всех публичных атрибутов объекта"""


def get_public_attributes(obj):
    return [arg for arg in dir(obj) if not arg.startswith('_')]


lst = [1, 2, 3, 3]
lst_gen = get_public_attributes(lst)
print(lst_gen)
my_string = 'bbvcdfghjkl'
str_gen = get_public_attributes(my_string)
print(str_gen)

