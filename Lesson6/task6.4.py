"""Написать функцию-генератор cycle которая бы возвращала циклический итератор."""
import itertools


def cycle(obj):
    if isinstance(obj, int):
        obj = [obj]
    if isinstance(obj, dict):
        for key, value in itertools.cycle(obj.items()):
            yield key, value
    for i in itertools.cycle(obj):
        yield i


#obj1 = [1, 2, 3, 4]
#obj1 = {'onion': 12, 'apple': 23, 'banana': 13, 'bread': 45, 'milk': 12}
obj1 = 123
result = cycle(obj1)
print(next(result))
print(next(result))
print(next(result))
print(next(result))
print(next(result))
print(next(result))
print(next(result))
