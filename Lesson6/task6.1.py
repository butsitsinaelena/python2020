"""Напишите итератор EvenIterator, который позволяет получить из списка все элементы,
стоящие на чётных индексах."""


class EvenIterator:

    def __init__(self, iter_list):
        self.iter_list = iter_list
        self.start = 0
        self.end = 2
        self.iter_numb = 2

    def __iter__(self):
        return self

    def __next__(self):
        if self.end <= len(self.iter_list) - 1:
            self.start = self.end
            self.end = self.start + self.iter_numb
            return self.iter_list[self.start]
        else:
            raise StopIteration


l1 = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11]
it = EvenIterator(l1)
while True:
    try:
        print(next(it))
    except StopIteration:
        print("\nStopIteration: List is the end")
        break
