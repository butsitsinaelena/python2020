def print_number(number_of_tests):
    list_numbers = []
    for i in range(number_of_tests):
        string_number = input('Enter two number: ')
        list_number = string_number.split(' ')
        list_numbers.append(list_number)
    return list_numbers


def get_division(number_of_tests):
    lst = print_number(number_of_tests)
    for elm in lst:
        try:
            print(int(elm[0]) / int(elm[1]))
        except ZeroDivisionError:
            print("Error Code: Integer division or modulo by zero")
        except ValueError:
            print('Error Code: Invalid literal for int() with base 10: {}'.format(elm[1]))


number = int(input('Enter number of tests: '))
get_division(number)

