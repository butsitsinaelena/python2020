"""Есть два списка разной длины, в одном ключи,
в другом значения.Составить словарь. Для ключей,
для которых нет значений использовать None в
качестве значения. Значения, для которых нет
ключей игнорировать."""
import itertools


def merge_dictionary(list_key, list_value):
    merged_dict = {}
    if len(list_key) >= len(list_value):
        for i in itertools.zip_longest(list_key, list_value):
            merged_dict[i[0]] = i[1]
        return merged_dict
    else:
        for i in zip(list_key, list_value):
            merged_dict[i[0]] = i[1]
        return merged_dict


lst1 = ['onion', 'apple', 'banana', 'bread', 'milk']
lst2 = [12, 23, 13, 45, 12, 34, 45]
lst3 = [23, 56, 7]
print(merge_dictionary(lst1, lst2))
print(merge_dictionary(lst1, lst3))
