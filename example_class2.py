class Person:
    def __init__(self, name, job=None, pay=0):  # азполняем поля записи name, job, pay -  локальные
        # переменные в области видимости ф-ии  __init__
        self.name = name  # т.е. создадим атрибуты обьекта
        self.job = job  # self  - указывает, что атрибут принадлежит экземпляру класса
        self.pay = pay  # self.pay -  атрибут экземпляра класса
        # self.pay = pay -  присваиваем экземпляру класса значение локаьной переменной, для дальнейшего использования
        # __init__ - конструктор класса, вызывается автоматически в момент создания экземпляра класса

    def last_name(self):
        return self.name.split()[-1]

    def give_raise(self, percent):
        self.pay = int(self.pay*(1 + percent))

    def __str__(self):
        return '[Person: %s, %s]' % (self.name, self.pay)


class Manager(Person):  # НАСЛЕДОВАНИЕ
    def give_raise(self, percent, bonus=0.10):  # переопределяем метод - АДАПТАЦИЯ
        #  self.pay = int(self.pay*(1 + percent + bonus))  # неправильно. при изменении нужно менять в 2 местах
        Person.give_raise(self, percent + bonus)  # правильно - дополняет оригинал,

    def some_thing_else(self):  # РАСШИРЕНИЕ
        pass


# if __name__ == '__main__':
#     bob = Person('Bob Smith')
#     sue = Person('Sue Jones', job='dev', pay=100000)
#     print(bob.name, bob.pay)  # из влекаем атрибуты класса
#     print(sue.name, sue.pay)
#     print(bob.last_name(), sue.last_name())
#     sue.give_raise(0.10)
#     print(sue.pay)
#     print(sue)
#     tom = Manager('Tom Jones', 'manager', 50000)
#     tom.give_raise(0.10)
#     print(tom.last_name())
#     print(tom)
#     print('--All_tree--')
#     for obj in (bob, sue, tom):  # полиморфизм
#         obj.give_raise(0.10)
#         print(obj)
##################################################################


class Person:
    def __init__(self, name, job=None, pay=0):
        self.name = name
        self.job = job
        self.pay = pay

    def last_name(self):
        return self.name.split()[-1]

    def give_raise(self, percent):
        self.pay = int(self.pay * (1 + percent))

    def __str__(self):
        return '[Person: %s, %s]' % (self.name, self.pay)


# class Manager(Person):
#     def __init__(self, name, pay):  # переопределение конструктора
#         Person.__init__(self, name, 'manager', pay)  # вызов оригинального конструктора со значением ' manager'
#     # в аргументе job
#
#     def give_raise(self, percent, bonus=0.10):
#         Person.give_raise(self, percent + bonus)
#
#     def some_thing_else(self):
#         pass
#
#
# if __name__ == '__main__':
#     bob = Person('Bob Smith')
#     sue = Person('Sue Jones', job='dev', pay=100000)
#     print(bob.name, bob.pay)  # из влекаем атрибуты класса
#     print(sue.name, sue.pay)
#     print(bob.last_name(), sue.last_name())
#     sue.give_raise(0.10)
#     print(sue.pay)
#     print(sue)
#     print('---------------Second variant----------------')
#     tom = Manager('Tom Jones', 50000)
#     tom.give_raise(.10)
#     print(tom.last_name())
#     print(tom)
#     print(tom.job)


#########данный вариант  не используют в реальной жизни###########################################

# class Manager:  # класс с вложенным обьектом
#     def __init__(self, name, pay):
#         self.person = Person(name, 'manager', pay)  # вложенный обьект Person
#
#     def give_raise(self, percent, bonus=0.10):
#         self.person.give_raise(percent + bonus)
#
#     def __getattr__(self, attr):  # перехватывает обращение к несуществующим атрибутам
#         return getattr(self.person, attr)  # делегирует эти обращения вложенному обьекту
#
#     def __str__(self):
#         return str(self.person)
#
#
# if __name__ == '__main__':
#     print('----------3 variant------------')
#     bob = Person('Bob Smith')
#     sue = Person('Sue Jones', job='dev', pay=100000)
#     print(bob.name, bob.pay)  # из влекаем атрибуты класса
#     print(sue.name, sue.pay)
#     print(bob.last_name(), sue.last_name())
#     sue.give_raise(0.10)
#     print(sue.pay)
#     print(sue)
#     tom = Manager('Tom Jones', 50000)
#     tom.give_raise(.10)
#     print(tom.last_name())
#     print(tom)
#     print(tom.job)


######### обьединие объектов в составной обект #########################


class Department:
    def __init__(self, *args):
        self.members = list(args)

    def add_member(self, person):
        self.members.append(person)

    def give_raises(self, percent):
        for person in self.members:
            person.give_raise(percent)

    def show_all(self):
        for person in self.members:
            print(person)


if __name__ == '__main__':
    bob = Person('Bob Smith')  # встраивание обьектов в составной обьект
    sue = Person('Sue Jones', job='dev', pay=100000)
    tom = Manager('Tom Jones', 'manager', 50000)
    development = Department(bob, sue)
    development.add_member(tom)
    development.give_raises(.10)  # вызов метода give_rause  вложенных обьектов
    development.show_all()  # вызов метода __str__ вложенных обьектов
