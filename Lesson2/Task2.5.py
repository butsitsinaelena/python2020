"""5. Написать программу, которая принимает от пользователя имя и пароль. Сравнивает пароль с заданным в коде.
В случае совпадения печатает в консоль "Password for user: <Имя пользователя> is correct"
Если пароль не совпадает, то печатает в консоль
"Password for user: <Имя пользователя> is incorrect"
"Please try again..."
И снова запрашивает пароль (не завершается)."""
import hashlib
import base64


def hashing_pass(password):
    hash_object = hashlib.sha512(password.encode())
    hash_pass = base64.b64encode(hash_object.digest())
    return hash_pass


def check_password():
    # Для безопасности пароль захеширован
    password_dict_hash = {
        'Anna': b'wYZB28myKzV3OzuDKSUTUC0eLuGGgLHNi02ploauq8zk2cBrCcAUbbsmpjxB66cObmIZwNASrYmqHm4SbZZQcA==',
        'Fred': b'AaO+RzGqDUAdLkirhOYzu7O5riiTS7DWV17ARrYSHYG19coQKoBLzVXXfFhzfYcE5d7jeruRpXxszD23yjKfgQ==',
        'Gru': b'lxzOtFUwrz6Gr7jbWIia+qjrOkIUSL3lBXjL4VNorWhXFjNKkO3UtBJi26tFzRF+QiHr4q42mjzCEuHQvAnuHw=='}
    while True:
        name = input('Please, enter your name: ')
        if name in password_dict_hash:
            while True:
                password_str = input('Please, enter your password: ')
                password = hashing_pass(password_str)
                if password == password_dict_hash.get(name):
                    print("Password for user: {} is correct".format(name))
                    exit(0)
                else:
                    print("Password for user: {} is incorrect'".format(name))
                    print("Please try again...")
                    continue
        else:
            print("User with the name: {} does not exist'".format(name))
            continue


check_password()

# Имена и пароли
# password_dict = {'Anna': 'dvery12', 'Fred': 'fghk34', 'Gru': 'gru118gru'}
# Anna_password = hashing_pass('dvery12')
# Fred_password = hashing_pass('fghk34')
# Gru_password = hashing_pass('gru118gru')
# print(Anna_password)
# print(Fred_password)
# print(Gru_password)
