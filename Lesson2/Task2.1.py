"""1. Написать функцию, которая печатает квадраты всех нечетных чисел в произвольном интервале [0, Х]. А так же
количество таких чисел."""


def print_number(n):
    count = 0
    for i in range(0+1, n+1, 2):
        print(i**2)
        count += 1
    print('The number of squares of odd numbers = ', count)


try:
    stop = int(input('enter stop: '))
except ValueError:
    print("Number should be integer")
try:
    print_number(stop)
except NameError:
    print('Error occurred')
    exit(1)
