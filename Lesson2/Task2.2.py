"""2. Написать функцию, которая принимает 3 числа (a,b,c) и проверяет сколько чисел между ‘a’ и ‘b’ делятся на ‘c’"""


def check_slice(num1, num2, num3):
    if num3 == 0:
        return 'Third number should not be 0'
    count = 0
    if num1 < num2:
        for i in range(num1 + 1, num2):
            if i % num3 == 0:
                count += 1
        return count
    else:
        for i in range(num2 + 1, num1):
            if i % num3 == 0:
                count += 1
        return count


try:
    a = int(input('Enter first number: '))
    b = int(input('Enter second number: '))
    c = int(input('Enter third number: '))
except ValueError:
    print("Numbers should be integers")
    exit(1)

my_count = check_slice(a, b, c)
print(my_count)
