"""4. Написать свою имплементацию функции range() из Python 2.x (аналогично Python 3,
только возвращает список)."""


def get_range_list(start, stop, step):
    range_list = []
    while True:
        if start < stop:
            range_list.append(start)
            start = start + step
        else:
            return range_list


def my_range(start=None, stop=None, step=1):
    if start is not None and stop is None:
        stop, start = start, 0
    if step == 0:
        raise ValueError('step not be zero')
    elif step > 0:
        if start <= stop <= 0 or 0 <= start <= stop or start < 0 < stop:
            return get_range_list(start, stop, step)
        else:
            return list()
    else:
        if stop <= start <= 0 or 0 <= stop <= start or stop < 0 < start:
            return get_range_list(start, stop, step)
        else:
            return list()


print('my_range(5)= expected:[0, 1, 2, 3, 4] = actual: ', my_range(5))
print('my_range(-10)=[]:expected: [] = actual: ', my_range(-10))
print('my_range(0)=[]: expected: [] = actual:', my_range(0))
print('my_range(0, 10, 3) = expected:[0, 3, 6, 9] = actual:', my_range(0, 10, 3))
print('my_range(5, 10)= expected:[5, 6, 7, 8, 9] = actual:', my_range(5, 10))
print('my_range(5, 10, -3)= expected: [] = actual:', my_range(5, 10, -3))
print('my_range(-10, -6) = expected: [-10, -9, -8, -7] = actual: ', my_range(-10, -6))
print('my_range(-10, -100, -30)= expected: [-10, -40, -70] = actual: ', my_range(-10, -100, -30))
print('my_range(-100, -10, -30)= expected: [] = actual: ', my_range(-100, -10, -30))
print('my_range(-10, -100, 30)= expected: [] = actual:', my_range(-10, -100, 30))
print('my_range(-100, -10, 30)= expected:[-100, -70, -40] = actual:', my_range(-100, -10, 30))
print('my_range(-4, 10, 3)= expected:[-4, -1, 2, 5, 8] = actual:', my_range(-4, 10, 3))
print('my_range(-4, 4)= expected:[-4, -3, -2, -1, 0, 1, 2, 3] = actual:', my_range(-4, 4))
print('my_range(10, -6, -2)= expected:[10, 8, 6, 4, 2, 0, -2, -4] = actual:', my_range(10, -6, -2))
print('my_range(0, -5, -1)= expected:[0, -1, -2, -3, -4] = actual:', my_range(0, -5, -1))
print('my_range(-4, 0)= expected:[-4, -3, -2, -1] = actual:', my_range(-4, 0))
print('my_range(-4, 0, -1)= expected: [] = actual:', my_range(-4, 0, -1))
print('my_range(4, 0)= expected: [] = actual:', my_range(4, 0))
print('my_range(-10, -10, 1)= expected: [] = actual:', my_range(-10, -10, 1))
print('my_range(110, 10)= expected: [] = actual:', my_range(110, 10))
print('my_range(10, -6, 2)= expected: [] = actual:', my_range(10, -6, 2))
print('my_range(0, 0, 0)= expected: "step not be zero" = actual: ', my_range(0, 0, 0))
