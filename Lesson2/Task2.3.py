"""3. Написать функцию вычисления факториала числа"""


def factorial_v1(number):
    if number < 0:
        return 'Number should no be < 0'
    if number == 0:
        my_factorial = 1
    else:
        my_factorial = 1
        for i in range(1, number+1):
            my_factorial = my_factorial * i
    return my_factorial


def factorial_v2(number):
    if number < 0:
        return 'Number should no be < 0'
    if number == 0:
        return 1
    return factorial_v2(number-1) * number


try:
    n = int(input('enter number: '))
except ValueError:
    print("Number should be integer")
    exit(1)

factor1 = factorial_v1(n)
factor2 = factorial_v2(n)
print(factor1)
print(factor2)
