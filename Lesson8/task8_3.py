# /home/test/python2020/Lesson8/task8_3.py
""" Напишите функцию, которая получает два аргумента
1)путь к файлу изображения jpeg на компьютере (строка);
2)имя целевого файла (строка)
отправляет файл HTTP POST запросом на
https://postman-echo.com/post .
В ответе будет получен файл изображения jpeg, в
виде octet-stream, который нужно раскодировать и
сохранить на компьютере под целевым именем,
переданным в аргументе.
Функция должна вернуть размер сохраненного файла."""
import os
import base64
import requests


def send_file_get_back(file_name, new_file_name):
    """ Send file and receive back
    :param file_name: file name to send
    :param new_file_name: received file name
    :return: size received file
    """
    upload_url = 'https://postman-echo.com/post'
    files = {'photo': open(file_name, 'rb')}
    res = requests.post(upload_url,
                        files=files)
    if res.status_code == 200:
        cont = res.json()
        received_file = cont['files'][str(file_name)]
        encoded_picture = received_file.split(',')
        decoded_picture = base64.b64decode(encoded_picture[1])
        open(new_file_name, 'wb').write(decoded_picture)
        return os.path.getsize(new_file_name)
    else:
        return 'Status code is not 200'


if __name__ == '__main__':
    ADDR = 'picture.jpg'
    FN = 'new_picture.jpg'
    print(send_file_get_back(ADDR, FN))
