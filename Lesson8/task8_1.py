# /home/test/python2020/Lesson8/task8_1.py
"""1. Напишите функцию, которая возвращает размер
HTML документа по адресу https://google.com.
Т.е. нужно получить страницу и вернуть ее размер
(количество символов)."""

import requests


def get_size(address):
    """ Function determining the size of an HTML document
    :param address: url address
    :return: document size number of characters
    """
    response = requests.get(address).content
    str_response = response.decode('utf-8')
    return len(str_response)


URL = 'https://google.com'
SIZE_STRING = get_size(URL)
print('{} size = {} characters'.format(URL, SIZE_STRING))
