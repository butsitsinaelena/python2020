"""Напишите шаблон регулярного выражения, который соответствует вопросительным
предложениям, в которых одно слово (более 2 символов) повторяется 4 или более раз."""
import re


sentence = 'Some Text! How are you, You, you, and again you and me, and Elena? Other Text!!'
regexp = '(?i)[^!.?]+\\b(\\w{2,})\\b(?:.*\\b\\1\\b){3}[^?]*\\?'
print(re.search(regexp, sentence))
