# /home/test/python2020/Lesson8/task8_2.py
""" Напишите функцию, которая принимает три аргумента
1)число, количество денег в исходной валюте, float;
2)исходная валюта, трехсимвольная строка, str;
3)целевая валюта, трехсимвольная строка, str;
и возвращает количество денег в целевой валюте
(тип float). Для получения курса валют
 воспользуйтесь https://api.exchangerate-api.com ."""
import requests


def transfer_currency(amount_of_money, source_currency, target_currency):
    """Function performs currency transfer/
    :param amount_of_money: amount of money, float
    :param source_currency: source currency
    :param target_currency: target currency
    :return: exchanged money
    """
    url = 'https://api.exchangerate-api.com/v4/latest/'
    url_base_currency = '{}{}'.format(url, source_currency)
    response = requests.get(url_base_currency)
    json_response = response.json()
    all_exchange_rates = json_response["rates"]
    exchange_rates = all_exchange_rates[target_currency]
    return amount_of_money * exchange_rates


print(transfer_currency(1000, 'RUB', 'USD'))

