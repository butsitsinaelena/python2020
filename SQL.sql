CREATE SCHEMA `shop` ;

CREATE TABLE `shop`.`category` (
  `id` INT NOT NULL,
  `name` VARCHAR(128) NOT NULL,
  `discount` INT NOT NULL,
  PRIMARY KEY (`id`));
  
  CREATE TABLE `shop`.`goods` (
  `vendor_code` INT NOT NULL,
  `brands_id` INT NOT NULL,
  `product_type_id` INT NOT NULL,
  `id categories` INT NOT NULL,
  `price` INT NOT NULL,
  PRIMARY KEY (`vendor_code`));
  
      CREATE TABLE `shop`.`type of goods` (
  `id` INT NOT NULL,
  `type` VARCHAR(128) NOT NULL,
  PRIMARY KEY (`id`));
  
      CREATE TABLE `shop`.`brand` (
  `id` INT NOT NULL,
  `brand` VARCHAR(128) NOT NULL,
  PRIMARY KEY (`id`));
  
use shop;
INSERT INTO `shop`.`category` (`id`, `name`, `discount`, `aliase_name`) VALUES ('1', 'Woman\'s clothing', 5, NULL);
INSERT INTO `shop`.`category` (`id`, `name`, `discount`, `aliase_name`) VALUES ('2', 'Man\'s clothing', 0, NULL);
INSERT INTO `shop`.`category` (`id`, `name`, `discount`, `aliase_name`) VALUES ('3', 'Woman\'s shoeses', 10, NULL);
INSERT INTO `shop`.`category` (`id`, `name`, `discount`, `aliase_name`) VALUES ('4', 'Man\'s shoeses', 15, 'man''s shoes');
ALTER TABLE `shop`.`category` 
CHANGE COLUMN `id` `id` INT(11) NOT NULL AUTO_INCREMENT ;
INSERT INTO `shop`.`category` (`name`, `discount`) VALUES ('hats', 0);
use shop;
ALTER TABLE `shop`.`category` 
CHANGE COLUMN `id` `id` INT(11) NOT NULL AUTO_INCREMENT ;
INSERT INTO category (`name`, `discount`) VALUES ('caps', 11);

CREATE TABLE `shop`.`order` (
id INT NOT NULL AUTO_INCREMENT,
user_name VARCHAR(128) NOT NULL,
phone VARCHAR(37) NOT NULL,
date_time DATETIME NOT NULL,
PRIMARY KEY (`id`));