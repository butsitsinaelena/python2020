-- create tables: CEO

CREATE TABLE company.CEO (
id_boss INT,
id_worker INT,
PRIMARY KEY (id_boss, id_worker),
CONSTRAINT fk_id_boss_employee
FOREIGN KEY (id_boss)
REFERENCES company.employee (id_employee)
ON DELETE NO ACTION
ON UPDATE NO ACTION,
CONSTRAINT fk_id_worker
FOREIGN KEY (id_worker)
REFERENCES company.employee (id_employee)
ON DELETE NO ACTION
ON UPDATE NO ACTION);

INSERT INTO company.CEO (id_boss, id_employee) VALUES (1, 2);
INSERT INTO company.CEO (id_boss, id_employee) VALUES (1, 2);
INSERT INTO company.CEO (id_boss, id_employee) VALUES (1, 1111);
INSERT INTO company.CEO (id_boss, id_employee) VALUES (1, 1334);
INSERT INTO company.CEO (id_boss, id_employee) VALUES (1, 2323);
INSERT INTO company.CEO (id_boss, id_employee) VALUES (1, 2325);
INSERT INTO company.CEO (id_boss, id_employee) VALUES (2, 3);
INSERT INTO company.CEO (id_boss, id_employee) VALUES (3, 1111);
SELECT * FROM company.employee;

