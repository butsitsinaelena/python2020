""" 10. Write a Python program to get the difference between the two lists """


# V1 - симметричная разница
def get_difference_v1(list1, list2):
    dif_list = []
    list2_copy = list2[:]
    if len(list1) == 0:
        dif_list = list2
    elif len(list2) == 0:
        dif_list = list1
    for i in range(len(list1)):
        if list1[i] not in list2:
            dif_list.append(list1[i])
        else:
            list2_copy.remove(lst1[i])
    dif_list.extend(list2_copy)
    return dif_list


# V2 показывает какие эл-ты есть в списке 1 и нет в списке 2. Если в списке 2 есть элементы, которых нет в списке1,
# то они будут утеряны
def get_difference_v2(list1, list2):
    diff = []
    for elm in list1:
        if elm not in list2:
            diff.append(elm)
    return diff


# V3 показывает какие эл-ты есть в списке 1 и нет в списке 2 с помощью множества
def get_difference_v3(list1, list2):
    return list(set(list1) - set(list2))


lst1 = [6, 7, 9, 34, 10, 1, 'nghh', 113]
lst2 = [6, 7, 9, 10, 1, 1, 6, 6, 6, 'nghh', 45, 'ert']
lstv1 = get_difference_v1(lst1, lst2)
lstv2 = get_difference_v2(lst1, lst2)
lstv3 = get_difference_v3(lst1, lst2)
print('v1___', lstv1)
print('v2___', lstv2)
print('v3___', lstv3)
