"""9. Write a Python program to remove duplicates from a list"""


def del_duplicates(my_list):
    unique_list = list(set(my_list))
    return unique_list


my_lst = [3, 3, 4, 5, 6, 1, 1, 'ddd', 'ddd', 'a']
unique_lst = del_duplicates(my_lst)
print(unique_lst)

