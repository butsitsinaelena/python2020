"""7. Write a Python script to merge two Python dictionaries"""


def merge_dict_v1(dict1, dict2):  # variant 1
    merge_dict = {}
    for key, value in dict1.items():
        if key not in dict2.keys():
            merge_dict[key] = value
        else:
            if value != dict2.get(key):
                merge_dict[key] = [value, dict2.get(key)]
            else:
                merge_dict[key] = value
    for key, value in dict2.items():
        if key not in dict1.keys():
            merge_dict[key] = value
    return merge_dict


def merge_dict_v2(dict1, dict2):   # variant 2
    dict3 = dict1.copy()
    dict3.update(dict2)
    return dict3


def merge_dict_v3(dict1, dict2):  # variant 3
    dict3 = {**dict1, ** dict2}
    return dict3


d1 = {1: 'mam', 2: 'pap', 3: 'ch1', 4: 'ch2', 5: 5, 't': [1, 3, 5], 67: {12, 45}, 23: {'d': 123, 't': 36}}
#d1 = {}
d2 = {5: 5, 6: 6, 7: 7, 2: 1, 't': [1, 3, 5], 67: {12, 45, 56}}

print('v1________________')
d_merge1 = merge_dict_v1(d1, d2)
print('merge dictionary v1: ', d_merge1)
print('\nv2________________')
d_merge2 = merge_dict_v2(d1, d2)
print('merge dictionary v2: ', d_merge2)
print('\nv3________________')
d_merge3 = merge_dict_v3(d1, d2)
print('merge dictionary v3: ', d_merge3)

