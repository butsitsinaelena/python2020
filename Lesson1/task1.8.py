""" 8. Write a Python program to find the highest 3 values in a dictionary """


# программа написана с расчетом на то, что все значания словаря цифры
def find_max_value(dictionary):
    return sorted(dictionary.values())[-1:-4:-1]


def find_max_value_v2(dictionary):
    return sorted(dictionary.values(), reverse=True)[0:3]


my_dict = {'f': 23, 'g': 134.8, 'w': 1115, 1: 2345, 3: 345, 'y': 1000, 'ff': 1115.9}
#my_dict = {}
#my_dict = {'f': 23}
max_lst = find_max_value(my_dict)
max_lst2 = find_max_value_v2(my_dict)
print('max_v1 = ', max_lst)
print('max_v2 = ', max_lst2)




