"""6. Write a Python script to generate and print a dictionary that contains a number (between 1 and n) in the
form (x, x*x).
Sample Dictionary ( n = 5) :
Expected Output : {1: 1, 2: 4, 3: 9, 4: 16, 5: 25} """


def create_dict_v1(numb):  # вариант 1
    my_dict = {}
    for key in range(1, numb+1):
        value = key * key
        my_dict[key] = value
    return my_dict


def create_dict_v2(numb):  # вариант 2
    my_dict = {key: key ** 2 for key in range(1, numb+1)}
    return my_dict


try:
    number = int(input('enter number: '))
except ValueError:
    print("number should not be integer")
    exit(1)


new_dict1 = create_dict_v1(number)
new_dict2 = create_dict_v2(number)
print('v1________________')
print(new_dict1)
print('\nv2________________')
print(new_dict2)