"""2. Write a Python program to count the number of characters (character frequency) in a string.
Sample String : google.com
Expected Result : {'o': 3, 'g': 2, '.': 1, 'e': 1, 'l': 1, 'm': 1, 'c': 1}"""
import collections


def count_letters(my_string):
    my_list = list(my_string)
    collect_counter = collections.Counter(my_list)
    letter_counter = dict(collect_counter.most_common())
    return letter_counter


my_str = input('Enter string: ')
count_let = count_letters(my_str)
print(count_let)

