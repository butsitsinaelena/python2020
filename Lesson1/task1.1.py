"""1. You are asked to ensure that the first and last names of people begin with a capital letter in their passports.
For example, alison heck should be capitalized correctly as Alison Heck.
Given a full name, your task is to capitalize the name appropriately.
Input Format:A single line of input containing the full name, S.
Constraints:* 0 < len(S) < 1000*
The string consists of alphanumeric characters and spaces.
Note: in a word only the first character is capitalized.
Example 12abc when capitalized remains 12abc.
Output Format:Print the capitalized string, S."""


# Программа расчитана на возможные случаи, когда полное имя состоит из одного слова(имя или фамилия)
def fix_full_name(full_name):
    if 0 < len(full_name) < 1000:
        if ' ' not in full_name:
            return full_name.capitalize()
        else:
            full_name = full_name.split(' ')
            name = full_name[0].capitalize()
            surname = full_name[1].capitalize()
            return '{} {}'.format(name, surname)
    else:
        return 'full name not correct'


passport_name = input('enter string: ')
correct_passport_data = fix_full_name(passport_name)
print(correct_passport_data)
