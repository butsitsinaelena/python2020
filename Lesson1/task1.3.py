"""3. Write a Python program to get a string made of the first 2 and the last 2 chars from a given a string.
If the string length is less than 2, return instead of the empty string.
Sample String : 'w3resource'
Expected Result : 'w3ce'
Sample String : 'w3'
Expected Result : 'w3w3'
Sample String : ' w'
Expected Result : Empty String"""


def create_string(my_string):
    if len(my_string) < 2:
        return ''
    else:
        return '{}{}{}'.format(my_string[0:2], my_string[-2], my_string[-1])


#long_string = 'w3resource'
#long_string = 'w3w3'
#long_string = 'w'
long_string = input('Enter string: ')
short_string = create_string(long_string)
print(short_string)
