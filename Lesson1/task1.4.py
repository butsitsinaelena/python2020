"""4. Write a Python program to count the number of strings where the string length is 2 or more and the first and last
character are same from a given list of strings.
Sample List : ['abc', 'xyz', 'aba', '1221']
Expected Result : 2"""


def count_str(my_list):
    count = 0
    for elm in my_list:
        if len(elm) >= 2 and elm[0] == elm[-1]:
            count += 1
    return count


my_lst = ['abc', 'xyz', 'aba', '1221', '12344', 'ee', '1cjkjghfhjkjh1', 12]
#my_lst = []
#my_lst = [1, 488, 343]
count_string = count_str(my_lst)
print(count_string)
