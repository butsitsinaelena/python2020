""" 5. You are given with 3 sets, find if third set is a subset of the first and the second sets
Input: set([1,2]), set([3,4), set([2])
Expected result: True
Input: set([1,2]), set([3,4), set([5])
Expected result: False"""


# my_set_1 = {1, 2}
# my_set_2 = {2, 3}
# my_set_3 = {2}

# my_set_1 = {1, 2}
# my_set_2 = {3, 4}
# my_set_3 = {5}

# my_set_1 = {1, 2}
# my_set_2 = {4, 5}
# my_set_3 = {2}

my_set_1 = {}
my_set_2 = {1}
my_set_3 = {}


def find_subset(set_1, set_2, set_3):
    if not set_3:
        return 'The third dictionary should not be empty'
    return set_3.issubset(set_1) and set_3.issubset(set_2)


print(find_subset(my_set_1, my_set_2, my_set_3))
